Render JPEG images progressive
==============================

Renders progressive JPEG files. Note that this works only on specific ImageMagick/GraphicsMagick versions. Test it!
There is pending bug in TYPO3 forge that adresses this: https://forge.typo3.org/issues/48013

This extension steals its code from there. Once the TYPO3 bug is merged (likely in another four years), then this
extension becomes obsolete. 

## Usage

Install and activate. Already generated images are not affected. Thus - use the clean up section in install tool to
remove and re-render those files.

## Features

* zero configuration, just install and done
* minimal system impact

## Bugs

If you find any bugs: Please report them.
