<?php

namespace VITD\JpegProgressive\Xclass\Core\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * Command Utility adaption, renders JPEGs progressive
 */
class CommandUtility extends \TYPO3\CMS\Core\Utility\CommandUtility
{
    public static function imageMagickCommand($command, $parameters, $path = '')
    {
        $gfxConf = $GLOBALS['TYPO3_CONF_VARS']['GFX'];
        $isExt = TYPO3_OS === 'WIN' ? '.exe' : '';
        if (!$path) {
            $path = $gfxConf['processor_path'];
        }
        $path = GeneralUtility::fixWindowsFilePath($path);
        // This is only used internally, has no effect outside
        if ($command === 'combine') {
            $command = 'composite';
        }
        // Compile the path & command
        if ($gfxConf['processor'] === 'GraphicsMagick') {
            $path = self::escapeShellArgument($path . 'gm' . $isExt) . ' ' . self::escapeShellArgument($command);
        } else {
            $path = self::escapeShellArgument($path . $command . $isExt);
        }
        // strip profile information for thumbnails and reduce their size
        if ($parameters && $command !== 'identify'
            && $gfxConf['processor_stripColorProfileByDefault']
            && $gfxConf['processor_stripColorProfileCommand'] !== ''
            && strpos($parameters, $gfxConf['processor_stripColorProfileCommand']) === false
        ) {
            // Determine whether the strip profile action has be disabled by TypoScript:
            if ($parameters !== '-version' && strpos($parameters, '###SkipStripProfile###') === false) {
                $parameters = $gfxConf['processor_stripColorProfileCommand'] . ' ' . $parameters;
            } else {
                $parameters = str_replace('###SkipStripProfile###', '', $parameters);
            }
        }

        // ---------------------- addition to upstream ----------------------
        if ($command !== 'identify') {
            $parameters = '-interlace Plane ' . $parameters;
        }
        // ------------------------------------------------------------------

        $cmdLine = $path . ' ' . $parameters;
        // It is needed to change the parameters order when a mask image has been specified
        if ($command === 'composite') {
            $paramsArr = GeneralUtility::unQuoteFilenames($parameters);
            $paramsArrCount = count($paramsArr);
            if ($paramsArrCount > 5) {
                $tmp = $paramsArr[$paramsArrCount - 3];
                $paramsArr[$paramsArrCount - 3] = $paramsArr[$paramsArrCount - 4];
                $paramsArr[$paramsArrCount - 4] = $tmp;
            }
            $cmdLine = $path . ' ' . implode(' ', $paramsArr);
        }
        return $cmdLine;
    }
}
