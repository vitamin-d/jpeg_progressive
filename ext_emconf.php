<?php
$EM_CONF['jpeg_progressive'] = [
    'title' => 'Render progressive JPEGs',
    'description' => 'Make TYPO3 render progressive JPEGs. Note that this works only on specific '
        . 'ImageMagick/GraphicsMagick versions. Test it! ',
    'category' => 'misc',
    'author' => 'Ludwig Rafelsberger',
    'author_email' => 'ludwig.rafelsberger@vitd.at',
    'author_company' => 'VITAMIN D GmbH',
    'state' => 'stable',
    'version' => '87.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.7.99',
        ],
    ],
];
