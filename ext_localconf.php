<?php
defined('TYPO3_MODE') or die();

(function ($packageKey) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Core\Utility\CommandUtility::class] = [
        'className' => \VITD\JpegProgressive\Xclass\Core\Utility\CommandUtility::class,
    ];
})('jpeg_progressive');
